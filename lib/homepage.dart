// ignore_for_file: use_key_in_widget_constructors

import 'package:chatty/chattile.dart';
import 'package:chatty/message.dart';
import 'package:chatty/theme.dart';
import 'package:flutter/material.dart';
// ignore_for_file: prefer_const_constructors

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blue,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => MessagePage()),
  );
        },
        backgroundColor: bgGreen,
        child: Icon(
          Icons.add,
          size: 28,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 40),
                Image.asset(
                  'assets/pic.png',
                  width: 100,
                  height: 100,
                ),
                SizedBox(height: 20),
                Text(
                  'Shabrina Carpenter',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  'Travel Freelancer',
                  style: TextStyle(
                    color: lightBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(30),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(48),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Friends',
                        style: TextStyle(
                            color: blacktext,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      ChatTile(
                        imageUrl: 'assets/pic2.png',
                        name: 'Haidar',
                        pesan: 'Sorry you are not my ty...',
                        date: 'Now',
                        unread: true,
                      ),
                      ChatTile(
                        imageUrl: 'assets/pic3.png',
                        name: 'Raissa',
                        pesan: 'I have crush on yo...',
                        date: '2.30',
                        unread: false,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Groups',
                        style: TextStyle(
                            color: blacktext,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      ChatTile(
                        imageUrl: 'assets/icon.png',
                        name: 'Semarang Fair',
                        pesan: 'have you ever see...',
                        date: '11.11',
                        unread: true,
                      ),
                      ChatTile(
                        imageUrl: 'assets/icon.png',
                        name: 'Grup Pinjol SMG',
                        pesan: 'DICARI ORANG BER...',
                        date: '11.11',
                        unread: true,
                      ),
                      ChatTile(
                        imageUrl: 'assets/icon2.png',
                        name: 'OSIS/MPK',
                        pesan: 'ono sing reti lak...',
                        date: '7/11',
                        unread: false,
                      ),
                      ChatTile(
                        imageUrl: 'assets/icon3.png',
                        name: 'PKK',
                        pesan: 'arisan kemarin yg menang bu...',
                        date: '7/11',
                        unread: false,
                      ),
                      ChatTile(
                        imageUrl: 'assets/icon3.png',
                        name: 'MOBEL LEJEN',
                        pesan: 'BAN APA NI GAI...',
                        date: '7/11',
                        unread: false,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
