// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

Color blue = Color(0xff1F8DF5);
Color lightBlue = Color(0xff9ed0ff);
Color graytext = Color(0xff808BA2);
Color blacktext = Color(0xff2C3A59);
Color bgGray = Color(0xffEAEFF3);
Color bgGreen = Color(0xff29CB9e);

TextStyle titleTextStyle = TextStyle(
  color: blacktext,
  fontWeight: FontWeight.w500,
  fontSize: 16,
);

TextStyle subtitleTextStyle = TextStyle(
  color: graytext,
  fontWeight: FontWeight.w300,
);
