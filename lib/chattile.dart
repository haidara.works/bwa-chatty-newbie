// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:chatty/theme.dart';

class ChatTile extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String pesan;
  final String date;
  final bool unread;

  ChatTile(
      {required this.imageUrl,
      required this.name,
      required this.pesan,
      required this.date,
      required this.unread});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Image.asset(imageUrl),
          SizedBox(
            width: 12,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
                style: TextStyle(
                    color: blacktext,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                pesan,
                style: unread
                    ? subtitleTextStyle.copyWith(color: blacktext)
                    : subtitleTextStyle,
              )
            ],
          ),
          Spacer(),
          Text(
            date,
            style: subtitleTextStyle,
          )
        ],
      ),
    );
  }
}
