// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:chatty/theme.dart';
import 'package:flutter/material.dart';

class MessagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget receiverBubble({
      var imageUrl,
      var text,
      var time,
    }) {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Image.asset(
              imageUrl,
              width: 40,
            ),
            SizedBox(width: 12),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 12,
              ),
              decoration: BoxDecoration(
                color: Color(0xffEBEFF3),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20),
                  topLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    text,
                    style: subtitleTextStyle,
                  ),
                  SizedBox(height: 5),
                  Text(
                    time,
                    style: subtitleTextStyle,
                  ),
                ],
              ),
            )
          ],
        ),
      );
    }

    Widget senderBubble({
      var imageUrl,
      var text,
      var time,
    }) {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 12,
              ),
              decoration: BoxDecoration(
                color: Color(0xffFFFFFF),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    text,
                    style: subtitleTextStyle,
                  ),
                  SizedBox(height: 5),
                  Text(
                    time,
                    style: subtitleTextStyle,
                  ),
                ],
              ),
            ),
            SizedBox(width: 12),
            Image.asset(
              imageUrl,
              width: 40,
            ),
          ],
        ),
      );
    }

    Widget header() {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.all(30),
        height: 115,
        decoration: BoxDecoration(
          color: Color(0xffFFFFFF),
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/icon3.png',
              width: 55,
              height: 55,
            ),
            SizedBox(
              width: 12,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Semarang Fair',
                  style: titleTextStyle,
                ),
                SizedBox(height: 2),
                Text(
                  '14,209 members',
                  style: subtitleTextStyle,
                ),
              ],
            ),
            Spacer(),
            Icon(
              Icons.call,
              size: 30,
              color: bgGreen,
            )
          ],
        ),
      );
    }

    Widget body() {
      return Expanded(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 30,
          ),
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              receiverBubble(
                imageUrl: 'assets/pic.png',
                text: 'How are ya guys ?',
                time: '2:30',
              ),
              receiverBubble(
                imageUrl: 'assets/pic.png',
                text: 'Come Here :P',
                time: '3:11',
              ),
              senderBubble(
                imageUrl: 'assets/pic3.png',
                text:
                    'Thinking about how to deal\n with this client from hell ...',
                time: '22:08',
              ),
              receiverBubble(
                imageUrl: 'assets/pic.png',
                text: 'Love them',
                time: '23:11',
              ),
            ],
          ),
        ),
      );
    }

    Widget chatInput() {
      return Container(
        width: MediaQuery.of(context).size.width - (2 * 30),
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: Color(0xffFFFFFF),
          borderRadius: BorderRadius.circular(75),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Type Something',
              style: subtitleTextStyle,
            ),
            // Image.asset(
            //   'assets/b.png',
            //   width: 35,
            // ),
            Icon(
              Icons.send,
              size: 35,
              color: bgGreen,
            ),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: Color(0xffF8FAFC),
      floatingActionButton: chatInput(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SafeArea(
        child: Column(
          children: [
            header(),
            body(),
          ],
        ),
      ),
    );
  }
}
